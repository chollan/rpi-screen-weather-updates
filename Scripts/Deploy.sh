#!/bin/bash

source Scripts/.env

sam package --template-file template.yaml  --output-template-file packaged.yaml --s3-bucket rgb-display-weather-updates-delivery
aws cloudformation deploy \
  --template-file packaged.yaml \
  --stack-name RgbWeatherDisplay \
  --capabilities CAPABILITY_NAMED_IAM \
  --parameter-overrides OpenWeatherApiAppId=$OPEN_WEATHER_APP_ID