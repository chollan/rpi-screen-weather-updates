const axios = require('axios');
const aws = require('aws-sdk');
const sqs = new aws.SQS();

exports.handler = async (event, context) => {

    // set some vars
    let url = "http://api.openweathermap.org/data/2.5/weather?id="+process.env.OPEN_WEATHER_LOCATION_ID+"&appid="+process.env.OPEN_WEATHER_APP_ID+"&units=imperial";
    //let url = "http://api.openweathermap.org/data/2.5/weather?id="+process.env.OPEN_WEATHER_LOCATION_ID+"&appid="+process.env.OPEN_WEATHER_APP_ID+"";
    let response;

    // get the weather
    try{
        response = await axios.get(url);
    }catch (e) {
        console.error(e);
        return;
    }

    // sent an update
    let update = {
        type:"weather",
        payload: {
            weather: response.data.main
        }
    };
    update.payload.weather.description = response.data.weather.map(row => {return row.description}).join(', ');

    await sqs.sendMessage({
        MessageBody: JSON.stringify(update),
        QueueUrl: process.env.RPI_SQS_QUE_URL,
        MessageDeduplicationId: Math.random().toString(),
        MessageGroupId: "1"
    }).promise();
    return true;
};